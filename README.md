**Eligiendo que dispositivo comprar**
===================
(Una guia practica?)

[TOC]

Antes de empezar
-------------------------
Antes de empezar a pensar en que dispositivo comprar, nos tenemos que preguntar por que lo queremos cambiar. Me parece que es una de las preguntas mas relevantes/importantes al momento de tomar esta decicion.

La respuesta no es relevante para la lectura del siguiente archivo, pero piensen sobre eso.

Entendamos tambien que en el mercado actualmente tenemos solamente tres sistemas operativos relevantes, Android, IOS y WindowsPhone.
Para los que me conocen saben que soy un fanatico de la serie de BlackBerry 10, tristemente fueron discontinuados a el publico masivo, por eso no lo cuento.

Como comentario fnal, no me importa que sea "lindo" fisicamene, eso es completamente subjetivo, ya que personalmente me gustan los dispositivos negros con la menor cantidad de detalles posibles.

Los distintos OS - pros & cons
----------

**IOS**

Pros:

 - Es altamente estable

 - La distibucion de actualizaciones es directa por Apple

 - Facil uso

 - La tienda de aplicaciones cuenta con una alta variedad de productos (entretenimiento y productividad).

 - Los productos en la tienda son verificados y validados por Apple, aportando mayor nivel de confiabilidad en las mismas.

 - Todo unificado dentro del mundo Apple con un ApplieId (Itunes/Icloud)

 - Buena velocidad de respuesta de aplicaciones/UI


Cons:

 - Desde el momento que se saca el dispositivo de la caja el mismo indica al usuario que hacer.

 - La experiencia de usuario puede resultar restrictiva

 - Bajo nivel de configuracion a nivel UI

 - Mala integracion con mundos no Apple (google/microsoft)

 - Las aplicaciones son en buena parte pagas y no hay forma (legal) de instalarlas por fuera del market

 - La beteria no tiene la mejor de las duraciones con uso intensivo

 
**Android**

Pros:

 - Altamente configurable a bnivel UI / Experiencia de usuario

 - Gran variedad de fabricantes de los cuales elegir. Personalmete recomiendo Sony, HTC o Blackberry (siendo esta una excelente opcion si se busca seguridad)

 - La tienda de aplicaciones cuenta con una cantidad incontable de publicaciones, y opciones, entre productos gratuitos y pagos., tanto de productividad y entretenimiento.

 - Se puede instalar aplicaciones de fuentes no seguras (publicadas fuera del Play Store) 

 - Integracion natural y automatica con el mungo Google usando un mail del mismo.

Cons:

 - Mala integracion con mundos no google (Microsoft/Apple) - Ejemplo para poder configurar una cuenta de 365 (estandar laboral) se requiere una aplicacion de Miscrosoft

 - Las aplicaciones en la tienda no llevan validacion por parte de google, por lo cual no se puede asegurar calidad o seguridad en las mismas.

 - La experiencia de usuario puede resultar confuza si se busca hacer cosas avanzadas. Android trabaja por Easter Eggs, escondiendo cosas, que pueden llegar a ser encontradas por casualidad

 - El OS gradualmente se ira degradando, volviendose mas lento e ineficiente. Requiere full reset esporadico (recomendable cada 6 meses)

 - Existen tantas varientes de OS como fabricantes de dispositivos ya que todos y cualquiera puede hacer modificaciones sobre el OS, lo cual puede derivar en inestabilidad del OS

 - Tiene inconvenientes con la sincronizacion de cuentas si se configuran mas de 5 cuentas de mail en el mismo equipo, su performance baja drasticamente
 
 
**Windows Phone**
Pros

 - Tiene un nivel de configuracion de UI medio

 - Integracion natural con mundo Microsoft.

 - Integracion madia con mundo Google (se pueden sincronizar contactos, cuentas y calendar, no asi Drive y otros productos Google)

 - La tienda de aplicaciones tiene buena variedad de aplicaciones de productividad y estan empezando a surgir de entretenimiento.

 - El OS es relativamente estable

 - Buena velocidad de respuesta de aplicaciones/UI

 - Los updates de OS son directos de Microsoft

 - Buen rendimiento general de la bateria


Cons

 - La tienda de aplicaciones no tiene ni la mitad de las que se pueden encontrar en cualquiera de los otros dos.

 - La integracion con mundo Apple es nula

 - Son dispositivos orientados mayormente a productividad

 - No existe gran variedad de dispositivos en el mercado actualmente, limitando las opciones de pantalla/procesdor/memoria y precio


Eleccion personal
-----------------------------
Personalmente he optado por dispositivos Microsoft, y mi decicion se basa en los siguientes criterios:

 - Para que voy a usar el dispositivo? - *Comunicaciones y productividad, para entretenimiento tengo una consola portatil de Nintendo*

 - Hay dispositivos con pantalla de 5.0 en adelante? - *Si*

 - La beteria puede durar mas de 24hs de uso intensivo? - *Si*

 - Puedo tener configuradas MUCHAS cuentas de mail y mensajeria simultaneamente? - *Si, actualmente hay 7 de mail, mas linkedin, Twitter, FB, y mail Laboral*

 - Hay un buen equipo en un rango de precio aceptable? - *Si*

Dado que mi celular es para mantenerme en contacto y trabajar, cumple su funcion de sobra. Anteriormente utilizaba BlackBerry10, pero como dije anteriormente fue discontinuado.


Otras consideraciones
------------------------
Fuera de las cuestiones tecnicas hay factores de apariencia y *status*, pero por favor, recordemos que hay fabricantes de dispositivos con Android que tienen equipos extremadamente "vistosos" y llamativos.

Finalmente jamas, bajo ningun concepto, recomiendo comprar Samsung, han estado bajamdo la calidad de sus productos, siendo el S3 o S4 el ultimo equipo que fabricaron de buena calidad, de ahi en adelante, se han mantenido arriba por una cuestion de marketing, no por hacer buenos equipos, y esot es una apreciacion personal.
Tampoco recomiendo comprar Motorolla si se utiliza mucho auriculares con cable, suelen tener un error de diseno, donde el plug del audio jack esta soldado a la plaqueta del dispositivo, en lugar de la carcaza, despues de utilizar el puerto de audio un tiempo de forma intensiva, el mismo se rompe, y rompe partes de la plaqueta, si, me paso.